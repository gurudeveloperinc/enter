<?php

namespace App\Http\Controllers;

use App\book;
use App\competition;
use App\competitionapplication;
use App\directory;
use App\event;
use App\internship;
use App\internshipapplication;
use App\student;
use App\User;
use App\video;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use PHPExcel_IOFactory;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
	    if(Auth::user()->role == "Staff") return redirect('/staff');
	    if(Auth::user()->role == "Admin") return redirect('/admin');
	    $competitions = competitionapplication::where('uid',Auth::user()->uid)->get();

	    $internships = internshipapplication::where('uid',Auth::user()->uid)->get();
        return view('home',[
	        'competitions' => $competitions,
	        'internships' => $internships
        ]);
    }


	public function staff() {

		$internships = internship::all()->sortByDesc("created_at");
		$competitions = competition::all()->sortByDesc("created_at");
		return view('staff',[
			'competitions' => $competitions,
			'internships' => $internships
		]);
	}

	public function admin() {
		$internships = internship::all()->where('uid',Auth::user()->uid)->sortByDesc("created_at");
		$competitions = competition::all()->where('uid',Auth::user()->uid)->sortByDesc("created_at");

		return view('admin',[
			'competitions' => $competitions,
			'internships' => $internships
		]);
	}

	public function getDirectory(){
		$directory = directory::all();

		return view('directory',[
			'directory' => $directory
		]);
	}

	public function viewIntReport() {
		$internships = internshipapplication::all();

		return view('viewIntReport',[
			'internships' => $internships
		]);
	}

	public function viewCompReport() {
		$competitions = competitionapplication::all();

		return view('viewCompReport',[
			'competitions' => $competitions
		]);
	}


	public function viewIntPDF() {
		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->viewIntReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("OIIE_Internship_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}

	}


	public function viewCompPDF() {
		try {
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml( $this->viewCompReport() );

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper( 'A4', 'portrait' );
			$dompdf->set_option('isRemoteEnabled', 'true');


			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream("OIIE_Competition_Report_" . Carbon::now()->toDateString());
		} catch(Exception $e){
			echo "Something went wrong. Please try again.";
		}

	}


	public function getAddBook(){
		return view('addBook');
	}

	public function getAddEvent() {
		return view('addEvent');
	}

	public function getAddVideo() {
		return view('addVideo');
	}

	public function viewStudents() {

		$students = User::where('role',"Student")->get();

		return view('viewStudents',[
			'students' => $students
		]);
	}

	public function viewProfile( $uid ) {
		$user = User::find($uid);

		return view('viewProfile',['student' => $user]);
	}

	public function viewAdmins() {
		$user = User::where('role',"Admin")->get();

		return view('viewAdmins',[
			'admins' => $user
		]);
	}

	public function deleteUser( $uid ) {
		User::destroy($uid);
		return redirect('/staff');
	}

	public function getBooks() {
		$books = book::all()->sortByDesc('created_at');
		$tags = book::all()->unique('tag');
		return view('books',[
			'books' => $books,
			'tags' => $tags
		]);
	}

	public function getEvents() {
		$events = event::all()->sortByDesc('created_at');
		$locations = event::all()->unique('location');

		return view('events',[
			'events' => $events,
			'locations' => $locations
		]);
	}

	public function getEventsByLocation( $location ) {
		$events = event::all()->where('location',$location)->sortByDesc('created_at');
		$locations = event::all()->unique('location');
		return view('events',[
			'events' => $events,
			'locations' => $locations,
			'location' => $location
		]);
	}

	public function getAddCompetition() {
		return view('addCompetition');
	}

	public function getInternships() {
		$qualificationQ = Input::get('qualification');
		$typeQ = Input::get('type');

		$internships = internship::all()->sortByDesc('created_at');
		$type = internship::all()->unique('type');
		$qualification = internship::all()->unique('qualification');

		if(isset($typeQ) && !empty($typeQ) ){
			$internships = internship::all()->where('type',$typeQ)
				->sortByDesc('created_at');
		}

		if(isset($qualificationQ) && !empty($qualificationQ) ){
			$internships = internship::all()
                 ->where('qualification',$qualificationQ)->sortByDesc('created_at');
		}
			return view('internships',[
			'internships' => $internships,
			'type' => $type,
			'qualification' => $qualification
		]);
	}

	public function getCompetitions(){

		$companyQ = Input::get('company');
		$typeQ = Input::get('type');
		$competitions = competition::all()->sortByDesc('created_at');

		$type = competition::all()->unique('type');
		$company = competition::all()->unique('company');

		if(isset($typeQ) && !empty($typeQ) ){
			$competitions = competition::all()->where('type',$typeQ)
			                         ->sortByDesc('created_at');
		}

		if(isset($companyQ) && !empty($companyQ) ){
			$competitions = competition::all()
			                         ->where('company',$companyQ)->sortByDesc('created_at');
		}

		return view('competitions',[
			'competitions' => $competitions,
			'type' => $type,
			'company' => $company
		]);

	}

	public function approveInt(Request $request,$intid) {

		$int = internshipapplication::find($intid);
		$int->status = "Approved";
		$int->staff = Auth::user()->uid;
		$int->save();

		$email = $int->Student->email;
		$company = $int->Internship->company;
		$studentName = $int->Student->name;
		$duration = $int->type;
		$date = $int->created_at;
		$contact = $int->Internship->contact;

		mail($email,"Your application for internship at $company has been approved",
		"Hi $studentName\nYour application for a $duration internship at $company which you made at $date has been approved.\n".
		"Please contact $contact for further details.\nThank you\nOIIE Support");

		$request->session()->flash('success',"Application Approved");

		return redirect('/view-int-applicants');
	}

	public function approveComp( Request $request, $compid ) {
		$int = competitionapplication::find($compid);
		$int->status = "Approved";
		$int->staff = Auth::user()->uid;
		$int->save();

		$email = $int->Student->email;
		$company = $int->Competition->company;
		$studentName = $int->Student->name;
		$duration = $int->type;
		$date = $int->created_at;
		$contact = $int->Competition->contact;

		mail($email,"Your application for a competition at $company has been approved",
			"Hi $studentName\nYour application for a $duration internship at $company which you made at $date has been approved.\n".
			"Please contact $contact for further details.\nThank you\nOIIE Support");

		$request->session()->flash('success',"Application Approved");

		return redirect('/view-comp-applicants');
	}

	public function rejectInt( Request $request, $intid ) {
		$int = internshipapplication::find($intid);
		$int->status = "Rejected";
		$int->staff = Auth::user()->uid;
		$int->save();

		$email = $int->Student->email;
		$company = $int->Internship->company;
		$studentName = $int->Student->name;
		$duration = $int->type;
		$date = $int->created_at;

		mail($email,"Your application for internship at $company has been rejected",
			"Hi $studentName\nYour application for a $duration internship at $company which you made at $date has been rejected.\n".
			"Don't give up, keep trying and you would get a placement.\nThank you\nOIIE Support");

		$request->session()->flash('success',"Application Rejected");

		return redirect('/view-int-applicants');

	}

	public function rejectComp(Request $request, $compid) {
		$int = competitionapplication::find($compid);
		$int->status = "Rejected";
		$int->staff = Auth::user()->uid;
		$int->save();

		$email = $int->Student->email;
		$company = $int->Competition->company;
		$studentName = $int->Student->name;
		$duration = $int->type;
		$date = $int->created_at;


		mail($email,"Your application for a competition at $company has been rejected",
			"Hi $studentName\nYour application for a $duration internship at $company which you made at $date has been approved.\n".
			"Don't give up, keep trying an you would be accepted into a competition.\nThank you\nOIIE Support");

		$request->session()->flash('success',"Application Rejected");

		return redirect('/view-comp-applicants');
	}

	public function getAddInternship() {
		return view('addInternship');
	}


	public function getUpdateDirectory() {

		return view('updateDirectory');
	}

	public function getVideos() {
		$videos =video::all();
		return view('videos',[
			'videos' => $videos
		]);
	}

	public function getProfile() {

		return view('profile');
	}


	public function getBooksByTags($tag){
		$books = book::all()->where('tag',$tag)->sortByDesc('created_at');
		$tags = book::all()->unique('tag');
		return view('books',[
			'books' => $books,
			'tags' => $tags,
			'tag' => $tag
		]);

	}

	public function postAddBook( Request $request ) {

		$fileName = $request->file('file')->getClientOriginalName();
		$request->file('file')->move("uploads/books",$fileName);


		$book = new book();
		$book->title = $request->input('title');
		$book->description = $request->input('description');

		if($request->hasFile('photo')){

			$photoName = $request->file('photo')->getClientOriginalName();
			$request->file('photo')->move("uploads/books/images",$photoName);

			$book->photo = url("uploads/books/images",$photoName);
		}
			else $book->photo = url('/images/book.png');

		$book->tag = $request->input('tag');
		$book->url = url('uploads/books/' . $fileName);
		$book->uid = Auth::user()->uid;
		$book->save();

		return view('addBook',['status' => "Book added successfully"]);
	}

	public function postAddEvent( Request $request ) {

		$event = new event();
		$event->title = $request->input('title');
		$event->description = $request->input('description');

		if($request->hasFile('photo')){

			$photoName = $request->file('photo')->getClientOriginalName();
			$request->file('photo')->move("uploads/events/images",$photoName);

			$event->photo = url("uploads/events/images",$photoName);
		}
		else $event->photo = url('/images/default.png');

		$event->location = $request->input('location');
		$event->date = $request->input('date');
		$event->time = $request->input('time');
		$event->uid = Auth::user()->uid;
		$event->save();

		$request->session()->flash("success", "Successfully added Event");
		return view('addEvent');
	}

	public function postAddInternship ( Request $request){

		$internship = new internship();
		$internship->type = $request->input('type');
		$internship->qualification = $request->input('qualification');
		$internship->company = $request->input('company');
		if($request->hasFile('photo')){

			$photoName = $request->file('photo')->getClientOriginalName();
			$request->file('photo')->move("uploads/events/images",$photoName);

			$internship->photo = url("uploads/events/images",$photoName);
		}
		else $internship->photo = url('/images/default.png');


		$internship->description = $request->input('description');
		$internship->contact = $request->input('contact');
		$internship->deadline = $request->input('deadline');
		$internship->uid = Auth::user()->uid;
		$internship->save();

		$request->session()->flash("success","Internship Listing Created");

		return redirect("/add-internship");
	}

	public function postAddCompetition ( Request $request){

		$competition = new competition();
		$competition->type = $request->input('type');
		$competition->company = $request->input('company');
		if($request->hasFile('photo')){

			$photoName = $request->file('photo')->getClientOriginalName();
			$request->file('photo')->move("uploads/competitions/images",$photoName);

			$competition->photo = url("uploads/competitions/images",$photoName);
		}
		else $competition->photo = url('/images/default.png');


		$competition->description = $request->input('description');
		$competition->contact = $request->input('contact');
		$competition->deadline = $request->input('deadline');
		$competition->uid = Auth::user()->uid;
		$competition->save();

		$request->session()->flash("success","Successfully created competition");

		return redirect("/add-competition");
	}

	public function internshipReadMore( $id ) {
		$intern = internship::find($id);
		return view('internshipReadMore',['intern'=> $intern]);
	}

	public function apply( Request $request, $intid ) {
		$int = new internshipapplication();
		$int->intid = $intid;
		$int->uid = Auth::user()->uid;
		$int->save();
		$request->session()->flash("success", "Application successful");
		return redirect('/internships');
	}

	public function applyComp( Request $request, $compid ) {
		$int = new competitionapplication();
		$int->compid = $compid;
		$int->uid = Auth::user()->uid;
		$int->save();
		$request->session()->flash("success", "Application successful");
		return redirect('/competitions');
	}


	public function viewIntApplicants() {

		$ints = internship::where('uid',Auth::user()->uid)->get();
		$intsArray = array();

		foreach($ints as $item){
			array_push($intsArray, $item->intid);
		}

		$internships = internshipapplication::whereIn('intid',$intsArray)->where('status','Applied')->get();

		return view('viewInternship',[
			'internship' => $internships
		]);
	}

	public function viewCompApplicants() {

		$ints = competition::where('uid',Auth::user()->uid)->get();
		$intsArray = array();

		foreach($ints as $item){
			array_push($intsArray, $item->compid);
		}

		$competitions = competitionapplication::whereIn('compid',$intsArray)->where('status','Applied')->get();

		return view('viewCompetition',[
			'competition' => $competitions
		]);
	}

	public function postUpdateDirectory(Request $request) {
		/** Create a new Excel5 Reader  **/
		try {

			$inputFileName = $request->file('file')->getClientOriginalName();
			$request->file('file')->move("uploads/directory",$inputFileName);


			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/directory/".$inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/directory/".$inputFileName);

			$sheet = $objPHPExcel->getActiveSheet();
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();


			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, TRUE, FALSE);

			foreach($rowData as $cell){

				$directory = new directory();
				$directory->name = $cell[0];
				$directory->type = $cell[1];
				$directory->email = $cell[2];
				$directory->phone = $cell[3];
				$directory->save();
			}


		}
		catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}

		$request->session()->flash("success", "Directory data uploaded");
		return redirect('/update-directory');

	}

	public function logout() {
		Auth::logout();

		return redirect('/login');
	}
}
