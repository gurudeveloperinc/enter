<?php

namespace App\Http\Controllers;

use App\student;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PublicController extends Controller
{
	public function index(){
		return view('welcome');
	}
	public function getStudentRegister() {
		return view('studentRegister');
	}

	public function postStudentRegister(Request $request) {

		$name = $request->input('fullname');
		$email = $request->input('email');
		$skill1 = $request->input('skill1');
		$skill2 = $request->input('skill2');
		$skill3 = $request->input('skill3');
		$facebook = $request->input('fackebook');
		$twitter = $request->input('twitter');
		$other = $request->input('other');
		$contact = $request->input('contact');
		$bio = $request->input('bio');
		$cv = "";
		$image = "";
		$cover = "";
		$password = Str::random(10);

		if($request->hasFile('cv')){
			$cv = $request->file('cv')->getClientOriginalName();
			$request->file('cv')->move('uploads/cvs',$cv);
			$cv = url('uploads/cvs/' . $cv);
		}

		if($request->hasFile('image')){
			$image = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads/images',$image);
			$image = url('uploads/images/' . $image);
		}

		if($request->hasFile('cover')){
			$cover = $request->file('cover')->getClientOriginalName();
			$request->file('cover')->move('uploads/images',$cover);
			$cover = url('uploads/images/' . $cover);
		}

		$user = new User();
		$user->name = $name;
		$user->email = $email;
		$user->password = bcrypt($password);
		$user->phone = $contact;
		$user->photo = $image;
		$user->save();

		$student = new student();
		$student->skill1 = $skill1;
		$student->skill2 = $skill2;
		$student->skill3 = $skill3;
		$student->facebook = $facebook;
		$student->twitter = $twitter;
		$student->other = $other;
		$student->bio = $bio;
		$student->cv = $cv;
		$student->cover = $cover;
		$student->uid = $user->uid;
		$student->save();

		mail($email, "OIIE - Your Login credentials", "Hello $name\nThank you for registering on OIIE website.Your login credentials are below\n\nEmail- $email\nPassword - $password");

		$request->session()->flash("success","Registration successful.Please login");
		return redirect('/login');

	}
}
