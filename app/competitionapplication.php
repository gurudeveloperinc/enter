<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class competitionapplication extends Model
{

	protected  $primaryKey = "compid";

	public function Competition() {
		return $this->belongsTo('App\competition','compid','compid');
	}

	public function Student() {
		return $this->hasOne('App\User','uid','uid');
	}

}
