<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class internshipapplication extends Model
{
    protected $primaryKey = "intid";

	public function Internship() {
		return $this->hasOne('App\internship','intid','intid');
	}

	public function Student() {
		return $this->hasOne('App\User','uid','uid');
	}
}
