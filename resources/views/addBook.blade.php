@extends('layouts.new')

@section('content')

    <div class="addBook" >
        @if( Session::has('success') )
            <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
        @endif

            @if( Session::has('error') )
                <div class="alert alert-error" align="center">{{Session::get('success')}}</div>
            @endif

            @include('staffSidebar')

            <section id="body-container" class="animsition dashboard-page">
                <div class="conter-wrapper">

                    <div id="preview" align="center" class="postImagePreview"></div>

                    <div class="col-md-12" style="width:70%; margin-left:160px;">
                        <div class="panel panel-success">

                            <div class="panel-heading">
                                <h3 class="panel-title">Add Book
                                    <div class="panel-control pull-right">
                                        <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                        <a class="panelButton"><i class="fa fa-minus"></i></a>
                                        <a class="panelButton"><i class="fa fa-remove"></i></a>
                                    </div>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form action="{{url('/add-book')}}" enctype="multipart/form-data" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <div class="row>">

                                            <label for="title">Title</label>
                                            <input type="text" name="title" class="form-control underline" id="title" placeholder="Book Title">



                                        </div>
                                    </div>



                                    <div class="row">


                                        <label for="description">Description</label>
                                        <textarea type="text" class="form-control underline" name="description" placeholder="Describe the book "></textarea>

                                    </div>



                                    <div class="form-group">
                                        <div class="col-md-12" style="margin-top:30px; margin-bottom:50px;">



                                            <div class="col-md-6">
                                                <label>Upload Book</label>



                                                <div class="row" >
                                                    <input name="file" type="file" class="btn btn-success btn-bordered" >



                                                </div>

                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="row>">

                                            <label for="tag">Tag</label>
                                            <input type="text" class="form-control underline" name="tag" id="tag" placeholder="Book Tag">



                                        </div>
                                    </div>

                                    <br><br>

                                    <div class="form-group">
                                        <div class="col-md-12" style="margin-top:30px; margin-bottom:50px;">



                                            <div class="col-md-6">
                                                <label>Book Cover Image</label>



                                                <div class="row">
                                                    <input name="photo" onchange="readURL(this);" type="file" class="btn btn-success btn-bordered" >



                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <br>

                                    <button type="submit" class="btn btn-success">Add New Book</button>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>

            </section>
    <script>

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<img src ="' + e.target.result + '">');
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>
@endsection