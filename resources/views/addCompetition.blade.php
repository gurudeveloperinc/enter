@extends('layouts.new')

@section('content')



        @if( Session::has('success') )
            <div class="green success centered" align="center">{{Session::get('success')}}</div>
        @endif

        @include('staffSidebar')

        <section id="body-container" class="animsition dashboard-page">
            <div class="conter-wrapper">



                <div class="col-md-12" style="width:70%; margin-left:160px;">
                    <div class="panel panel-success">

                        <div class="panel-heading">
                            <h3 class="panel-title">Add Competition
                                <div class="panel-control pull-right">
                                    <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                    <a class="panelButton"><i class="fa fa-minus"></i></a>
                                    <a class="panelButton"><i class="fa fa-remove"></i></a>
                                </div>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <form>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <label for="title">Company Name</label>
                                        <input type="text" class="form-control underline" id="title" placeholder="Title">
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-12" style="margin-top:30px; margin-bottom:50px;">
                                        <div class="col-md-6">
                                            <label for="type">Type</label>
                                            <select class="form-control">
                                                <option>3 Months</option>
                                                <option>6 Months</option>
                                                <option>1 Year</option>

                                            </select>
                                        </div>


                                        <div class="col-md-6">
                                            <label>Upload Image</label>



                                            <div class="row">
                                                <input name="image" type="file" class="btn btn-success btn-bordered" >



                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="description">Description</label>
                                        <textarea type="text" class="form-control underline" placeholder=""></textarea>

                                    </div>

                                </div>
                                <br><br><br>


                                <div class="row">
                                    <div class="col-md-12">


                                        <div class="col-md-4" style="margin-right:15px;">
                                            <label for="date">Contact Information</label>
                                            <input type="text" class="form-control underline" placeholder="Contact Number">
                                        </div>




                                        <div class="col-md-6" style="margin-left:95px;">
                                            <label for="date">Deadline</label>
                                            <p class="input-group">
                                            <div class="input-group date" id="datetimepicker">
                                                <input type="date" class="form-control" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
                                            </div>
                                            </p>
                                        </div>

                                    </div>

                                </div>
                                <br>

                                <button type="submit" class="btn btn-success">Add New Competition</button>
                            </form>
                        </div>
                    </div>


                </div>
            </div>

        </section>
    {{--<div class="ui form segment ten wide column centered">--}}

        {{--<h1 align="center">Add Competition</h1>--}}
        {{--<form method="post" enctype="multipart/form-data" action="{{url('/add-competition')}}">--}}
            {{--{{csrf_field()}}--}}
        {{--<div class="two fields">--}}
            {{--<div class="field">--}}
                {{--<label>Company Name</label>--}}
                {{--<input type="text" name="company" placeholder="Company Name">--}}
            {{--</div>--}}
            {{--<div id="preview" class="postImagePreview"></div>--}}

        {{--</div>--}}
        {{--<div class="ui divider"></div>--}}
        {{--<div class="two fields">--}}
            {{--<div class="field">--}}
                {{--<label>Type</label>--}}
                {{--<select name="type">--}}
                    {{--<div class="default text">Select</div>--}}
                    {{--<option>Select</option>--}}
                    {{--<option>3 months</option>--}}
                    {{--<option>6 months</option>--}}
                    {{--<option >1 year</option>--}}
                {{--</select>--}}

            {{--</div>--}}

            {{--<div class="field">--}}


                {{--<label>Add Image</label>--}}

                {{--<input onclick="readURL(this)" name="photo" type="file"  class="ui green">--}}
            {{--</div>--}}

        {{--</div>--}}
        {{--<div class="field">--}}
            {{--<label>Description</label>--}}
            {{--<textarea name="description"></textarea>--}}
        {{--</div>--}}
        {{--<div class="ui divider"></div>--}}

        {{--<div class="three required fields">--}}
            {{--<div class="field">--}}
                {{--<label>Contact Information</label>--}}
                {{--<input type="text" name="contact" placeholder="Contact Number">--}}
            {{--</div>--}}
            {{--<div class="field">--}}
                {{--<label>Deadline</label>--}}
                {{--<input type="text" name="deadline" placeholder="Eg. 27th May 2017">--}}
            {{--</div>--}}
            {{--<div class="ui divider"></div>--}}
        {{--</div>--}}
            {{--<div class="ui buttons">--}}
                {{--<button type="submit" class="ui positive button" >Add--}}
                {{--</button>--}}
            {{--</div>--}}

        {{--</form>--}}
    {{--</div>--}}


    <script>
        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').html('<img src ="' + e.target.result + '">');
                    };

                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>
@endsection