
@extends('layouts.new')

@section('content')


    @if( Session::has('success') )
            <div class="green success centered" align="center">{{Session::get('success')}}</div>
    @endif

    @include('staffSidebar')
    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper">



            <div class="col-md-12" style="width:50%; margin-left:200px;">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h3 class="panel-title">Add Event
                            <div class="panel-control pull-right">
                                <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                <a class="panelButton"><i class="fa fa-minus"></i></a>
                                <a class="panelButton"><i class="fa fa-remove"></i></a>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="{{url('/add-event')}}" enctype="multipart/form-data">

                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Event Title</label>
                                <input name="title" type="text" class="form-control underline" id="title" placeholder="Title">
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" class="form-control underline" id="description"></textarea>
                            </div>
                            <br>

                            <div class="row">
                                <label>Upload Image</label>
                                <br>
                                <div class="row" >
                                    <input name="photo" required type="file" class="btn btn-success btn-bordered">
                                </div>


                            </div>

                            <br>



                            <br>
                            <div class="row">


                                <div class="col-md-4">
                                    <label for="location">Location</label>
                                    <input type="text" class="form-control underline" required name="location" placeholder="Abuja">
                                </div>

                                <div class="col-md-4">
                                    <label for="date">Date</label>
                                    <input type="text" pattern="[0-9]{1,2}/[0-9]{1,2}/[0-9]{1,2}" class="form-control underline" name="date" required placeholder="10/5/17">
                                </div>

                                <div class="col-md-4">
                                    <label for="time">Time</label>
                                    <input type="text" pattern="[0-9]{1,2}:[0-9]{1,2}" class="form-control underline" name="time" placeholder="13:00" required>
                                </div>

                            </div>
                            <br>

                            <button type="submit" class="btn btn-success">Add New Event</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>

    </section>


    {{--<div class="ui form segment ten wide column centered">--}}
        {{--<form method="post" enctype="multipart/form-data" action="{{url('/add-event')}}">--}}

            {{--{{csrf_field()}}--}}
        {{--<div class="two fields">--}}
            {{--<div class="field">--}}
                {{--<label>Event Title</label>--}}
                {{--<input name="title" type="text" placeholder="Text Input">--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="ui divider"></div>--}}
        {{--<div class="field">--}}
            {{--<label>Description</label>--}}
            {{--<textarea name="description"></textarea>--}}

        {{--</div>--}}
        {{--<div class="ui divider"></div>--}}
        {{--<div class="field">--}}
            {{--<label>Add Images</label>--}}
            {{--<div>--}}
                {{--<input name="photo" type="file"  class="ui green button">Browse</div>--}}
        {{--</div>--}}
        {{--<div class="three required fields">--}}
            {{--<div class="field">--}}
                {{--<label>Location</label>--}}
                {{--<input name="location" type="text" placeholder="Text Input">--}}
            {{--</div>--}}

            {{--<div class="field">--}}
                {{--<label>Date</label>--}}
                {{--<input name="date" type="text" placeholder="Text Input">--}}
            {{--</div>--}}

            {{--<div class="field">--}}
                {{--<label>Time</label>--}}
                {{--<input name="time" type="text" placeholder="Text Input">--}}
            {{--</div>--}}
            {{--<div class="ui divider"></div>--}}
        {{--</div>--}}
            {{--<div class="ui buttons">--}}
                {{--<button type="submit" class="ui positive button">Add</button>--}}
            {{--</div>--}}

        {{--</form>--}}
    {{--</div>--}}


@endsection