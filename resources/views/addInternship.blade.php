@extends('layouts.new')

    @section('content')

        @include('staffSidebar')

    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper">



            <div class="col-md-12" style="width:70%; margin-left:160px;">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h3 class="panel-title">Add Internship
                            <div class="panel-control pull-right">
                                <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                <a class="panelButton"><i class="fa fa-minus"></i></a>
                                <a class="panelButton"><i class="fa fa-remove"></i></a>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">

                            <form method="post" enctype="multipart/form-data" action="{{url('/add-internship')}}">
                          {{csrf_field()}}
                            <div class="form-group">
                                <div class="row>">
                                    <div class="col-md-6">
                                        <label for="type">Type</label>
                                        <select name="type" class="form-control">
                                            <option>3 Months</option>
                                            <option>6 Months</option>
                                            <option>1 Year</option>

                                        </select>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="type">Qualification Level</label>
                                        <select name="qualification" class="form-control">
                                            <option>Waec Holder</option>
                                            <option>Undergraduate</option>
                                            <option>Graduate</option>
                                            <option>Youth Corper</option>

                                        </select>
                                    </div>

                                </div>
                            </div>




                            <div class="form-group">
                                <div class="col-md-12" style="margin-top:30px; margin-bottom:50px;">
                                    <div class="col-md-6">
                                        <label for="title">Company Name</label>
                                        <input type="text" name="company" class="form-control underline" id="title" placeholder="Title">
                                    </div>


                                    <div class="col-md-6">
                                        <label>Upload Image</label>

                                        <div class="row" >
                                            <input name="photo" type="file" class="btn btn-success btn-bordered">



                                        </div>

                                    </div>
                                </div>
                            </div>


                            <div class="row">


                                <label for="description">Description</label>
                                <textarea type="text" name="description" class="form-control underline" placeholder=""></textarea>

                            </div>


                            <br><br><br>


                            <div class="row">
                                <div class="col-md-12">


                                    <div class="col-md-4" style="margin-right:15px;">
                                        <label for="date">Contact Information</label>
                                        <input name="contact" type="text" class="form-control underline" placeholder="Contact Number">
                                    </div>




                                    <div class="col-md-6" style="margin-left:95px;">
                                        <label for="date">Deadline</label>
                                        <p class="input-group">
                                        <div class="input-group date" id="datetimepicker">
                                            <input name="deadline" type="date" class="form-control" />
					                    <span class="input-group-addon">
					                        <span class="glyphicon glyphicon-calendar"></span>
					                    </span>
                                        </div>
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <br>

                            <button type="submit" class="btn btn-success">Add New Internship</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>

    </section>

        @endsection