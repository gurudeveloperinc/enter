@extends('layouts.new')

@section('content')

    @include('staffSidebar')

    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper">



            <div class="col-md-12" style="width:70%; margin-left:160px;">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h3 class="panel-title">Add Video
                            <div class="panel-control pull-right">
                                <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                <a class="panelButton"><i class="fa fa-minus"></i></a>
                                <a class="panelButton"><i class="fa fa-remove"></i></a>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form>
                            <div class="form-group">
                                <div class="row>">

                                    <label for="title">Title</label>
                                    <input type="text" class="form-control underline" id="title" placeholder="Video Title">



                                </div>
                            </div>



                            <div class="row">


                                <label for="description">Description</label>
                                <textarea type="text" class="form-control underline" placeholder="Describe the video "></textarea>

                            </div>



                            <div class="form-group">
                                <div class="col-md-12" style="margin-top:30px; margin-bottom:50px;">



                                    <div class="col-md-6">
                                        <label>Upload Video</label>



                                        <div class="row" >
                                            <input name="image" type="file" class="btn btn-success btn-bordered" >



                                        </div>

                                    </div>
                                </div>
                            </div>









                            <br>

                            <button type="submit" class="btn btn-success">Add New Video</button>
                        </form>
                    </div>
                </div>


            </div>
        </div>

    </section>

@endsection