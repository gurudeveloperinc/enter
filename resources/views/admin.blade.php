@extends('layouts.new')
@section('content')
    @include('staffSidebar')
    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper home-container">
            <div class="cover-wrapper">
                <div class="cover-photo" style="background-image: url(
                @if (Auth::user()->role == "Student")
                {{Auth::user()->Student->cover}}
                @else
                        'images/profile-cover.jpg'
                @endif
                        );">

                    <div class="name-desg">
                        <h3>
                            DASHBOARD
                            <small>{{Auth::user()->role}}</small>
                        </h3>
                    </div>
                </div>
                <div class="profile-photo-warp">
                    <img class="profile-photo img-responsive img-circle" src="{{Auth::user()->photo}}" alt="">
                </div>

            </div>



            <div class="row home-row">
                <div class="col-lg-8 col-md-6">
                    <div class="map-container box padder">
                        <div style="width: 100%; height: 320px"><iframe
                                    width="640"
                                    height="320"
                                    frameborder="0" style="border:0"
                                    src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDmn2ZCyzm_LscerOxuOsbhewY4tVGMVL0
    &q=Office+for+ICT+Innovation+and+Entreprenuership,Maitama+Abuja" allowfullscreen>
                            </iframe></div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-3" style="margin-top:30px;">
                    <h3>All applications</h3>
                    <div class="home-stats">
                        <a class="stat hvr-wobble-horizontal">
                            <div class=" stat-icon">
                                <i class="fa fa-trophy fa-4x text-info "></i>
                            </div>
                            <div class=" stat-label">
                                <div class="label-header">
                                    {{count($competitions)}} competitions
                                </div>


                            </div>
                        </a>


                        <a class="stat hvr-wobble-horizontal">
                            <div class=" stat-icon">
                                <i class="fa fa-group fa-4x text-danger "></i>
                            </div>
                            <div class=" stat-label">
                                <div class="label-header">
                                    {{count($internships)}} internships
                                </div>


                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection