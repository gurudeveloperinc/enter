<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title> Login
    </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app-green.css" />
</head>
<body class="">
<div class="login-page">
    <div class="row">
        <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
            <img  src="images/oiie.png" class="user-avatar" style="width:300px;" />
            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
            @endif

            <h1>OIIE Start up network</h1>
            <form class="ui form" role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                <div class="form-content" >


                    <div class="form-group">
                        <input type="text" class="form-control input-underline input-lg" name="email" id="email" placeholder=email>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-underline input-lg" name="password" id="password" placeholder=password>


                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <input type="submit" class="btn btn-white btn-outline btn-lg btn-rounded progress-login" value="Login" />
                &nbsp;
                <a href="{{url('/student/register')}}" class="btn btn-white btn-outline btn-lg btn-rounded">Register</a>
            </form>
        </div>
    </div>
</div>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>
</body>

</html>




