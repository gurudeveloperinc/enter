@extends('layouts.new')

@section('content')

@if(Auth::user()->role == "Student")
    @include('studentSidebar')

@else
    @include('staffSidebar')


@endif



<?php use Carbon\Carbon; ?>


    <section id="body-container" class="animsition dashboard-page">
        <div class="row">
            @foreach($books as $item)
            <div class="col-sm-4 col-md-3" style="height: 200px;padding:30px;">
                <div class="thumbnail">
                    <img src="{{$item->photo}}" alt="images/flat-avatar.png">
                    <div class="caption">
                        <h3>{{$item->title}}</h3>
                        <p>{{$item->description}}</p>
                        <p>
                            <a href="{{$item->url}}" class="btn btn-primary" role="button" download>Download</a><br>
                            <small>Uploaded {{  Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}</small>

                        </p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>


        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </section>

    {{--<div class="ui three wide column">--}}


        {{--<div class="ui bulleted list">--}}

            {{--<h3 class="header">Tags</h3>--}}

            {{--<div class="item">--}}
                {{--<a href="{{url('/books/')}}">All</a>--}}
            {{--</div>--}}
            {{--@foreach($tags as $item)--}}
            {{--<div class="item">--}}
                {{--<a href="{{url('/books/' . $item->tag )}}" >{{$item->tag}}</a>--}}
            {{--</div>--}}
            {{--@endforeach--}}

        {{--</div>--}}

    {{--</div>--}}

    {{--<div class="ui books thirteen wide column centered cards">--}}

        {{--<h3 align="center">--}}
            {{--@if(isset($tag))--}}
            {{--{{$tag}}--}}
                {{--@else--}}
            {{--ALL--}}
            {{--@endif--}}
        {{--</h3>--}}


        {{--@foreach($books as $item)--}}

            {{--<div class="card" style="float:left">--}}
                {{--<div class="image">--}}
                    {{--<img src="{{$item->photo}}">--}}
                {{--</div>--}}
                {{--<div class="content">--}}
                    {{--<div class="header">{{$item->title}}</div>--}}
                    {{--<div class="meta">--}}
                        {{--<a>{{$item->tag}}</a>--}}
                    {{--</div>--}}
                    {{--<div class="description">--}}
                      {{--{{$item->description}}--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="extra content">--}}
                      {{--<span class="right floated">--}}
                          {{--Uploaded {{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}--}}

                      {{--</span>--}}
                      {{--<span>--}}

                    {{--<a href="{{$item->url}}" class="ui basic green button">Download</a>--}}
                      {{--</span>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@endforeach--}}

        {{--<div class="row" style="float: left;width:100%;">--}}
            {{----}}
            {{--<div class="ui divider"></div>--}}
            {{--<div align="center" class="ui buttons" id="next">--}}
                {{--<button class="ui button disabled">Previous</button>--}}
                {{--<button class="ui blue button">1</button>--}}
                {{--<button class="ui button">2</button>--}}
                {{--<button class="ui button">3</button>--}}
                {{--<button class="ui button">...</button>--}}
                {{--<button class="ui button">n</button>--}}
                {{--<button class="ui button">Next</button>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
@endsection