@extends('layouts.new')

@section('content')

    @if(Auth::user()->role == "Student")
    @include('studentSidebar')

    @else
        @include('staffSidebar')
    @endif


    <section id="body-container" class="animsition dashboard-page">
        <pageheader pagename="Table" subtitle="Bootstrap UI Elements"></pageheader>
        <div class="conter-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Directory
                                <div class="panel-control pull-right">
                                    <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                    <a class="panelButton"><i class="fa fa-minus"></i></a>
                                    <a class="panelButton"><i class="fa fa-remove"></i></a>
                                </div>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Contact Email</th>
                                    <th>Contact Number</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($directory as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->type}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->phone}}</td>

                                </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        </div>

        </div>

        </div>
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
@endsection