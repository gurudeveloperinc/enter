@extends('layouts.new')

@section('content')

    <aside id="sidebar">
        <div class="sidenav-outer">
            <div class="side-menu">
                <div class="menu-body">

                    @if(Auth::user()->role == "Student")
                        <ul class="left nav nav-pills nav-stacked sidenav">
                            <li >
                                <a href="{{url('/home')}}">
                                    <span class="glyphicon glyphicon-home"></span>
                                </a>
                                <ul class="nested-dropdown animated fadeIn">
                                    <li><a href="{{url('home')}}">Dashboard</a></li>
                                </ul>
                            </li>

                        </ul>
                    @endif
                    <div class="side-widgets">
                        <div class="widgets-content">
                            <div class="text-center">
                                <i class="user-avatar fa fa-users fa-5x"></i>
                                <div class="text-center avatar-name" style="margin-top:-40px;">
                                    Event
                                </div>
                            </div>



                            <div class="news-feed">
                                <div class="feed-header">LOCATIONS</div>
                                <div class="feed-content">
                                    <ul class="feed">
                                        <li>
                                            <a href="{{url('/events')}}">ALL</a>
                                        </li>
                                        @if(isset($locations))
                                            @foreach($locations as $item)
                                            <li>
                                                <a href="{{url('/events/'. $item->location)}}">{{$item->location}}</a>
                                            </li>
                                            @endforeach
                                        @endif


                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>







    <section id="body-container" class="animsition dashboard-page">
        <div class="row">
            @if(isset($location))
            <h2 style="position:relative; left:50%; ">{{$location}}</h2>
            @else
            <h2 style="position:relative; left:50%; ">ALL</h2>
            @endif

            @foreach($events as $item)
            <div class="col-sm-4 col-md-3">
                <div class="thumbnail">
                    <img src="{{$item->photo}}" alt="images/flat-avatar.png">
                    <div class="caption">
                        <h3>{{$item->title}}</h3>
                        <p>{{$item->description}}</p>
                        <p>{{$item->date}} | {{$item->time}}</p>
                        <span>{{$item->location}}</span>
                    </div>
                </div>
            </div>

            @endforeach

        </div>

        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </section>
@endsection