@extends('layouts.app')

@section('content')

    @include('staffSidebar')
    <section id="body-container" class="animsition dashboard-page">

        <div class="row" >
            <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12">
                <div class="tz-single-ticket">
                    <img src="images/laravel-shift.png" alt="Images" width="290" height="270">
                    <br><br>
                    <h3>

                        FOCAP INTERNSHIP
                    </h3>
                    <br>
                    <form class="tz-form-ticket tz-form-courses" name="frm_ticket" method="post" action="#">


                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-arrows-alt"></i>
                            APPLY
                        </button>
                    </form>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-xs-12 col-sm-12">
                <article class="tz-day-open">
                    <h3>

                        Internship Details
                    </h3>
                                    <span class="btn-success">

                                        Start: March 11, 2017 / Duration: 6 weeks
                                    </span>
                    <br>
                    <br>
                    <br>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque lobortis, nisl at ullamcorper egestas, felis tortor adipiscing dolor, a interdum lorem urna quis magna. Fusce diam eros, condimentum quis dictum at, blandit vel sem. Fusce pharetra vitae est a sollicitudin. Ut pharetra quam ac libero malesuada tincidunt. Etiam pulvinar velit ac accumsan mollis. Duis eu ornare nunc. Donec ultricies porttitor dolor eget semper. Sed quis pretium nunc. Integer auctor ullamcorper lobortis. Maecenas faucibus dui eget nibh lobortis dignissim. Nulla malesuada sem porta, tempus sem sed, dictum ante.</p>
                    <p>Fusce malesuada neque nisi, at commodo ipsum congue eu. Fusce id velit eros. Integer eget arcu sit amet mauris egestas dignissim. Integer gravida mauris nec justo tempus venenatis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nulla egestas facilisis magna. Ut non accumsan turpis. Nulla vitae urna faucibus tellus vehicula iaculis. Sed pretium dictum consequat. Curabitur ultricies dapibus leo. Etiam in vestibulum elit, quis dapibus purus. Vivamus posuere felis ac nisl dignissim auctor. Pellentesque molestie orci id tortor posuere volutpat.</p>
                    <p>Thank you.</p>

                </article>
            </div>

        </div>





    </section>

@endsection