@extends('layouts.new')

@section('content')

    
    <?php use Illuminate\Support\Facades\Input; ?>
<aside id="sidebar">
    <div class="sidenav-outer">
        <div class="side-menu">
            <div class="menu-body">

                <ul style="float:left;margin-top:20px;" class="nav nav-pills nav-stacked sidenav">
                    <li >
                        <a href="{{url('/home')}}">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('home')}}">Dashboard</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/directory')}}">
                            <span class="glyphicon glyphicon-duplicate"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('directory')}}">View Directory</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/events')}}">
                            <span class="glyphicon glyphicon-book"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/events')}}">View Events</a></li>
                        </ul>
                    </li>


                    <li >
                        <a href="{{url('/competitions')}}">
                            <span class="glyphicon glyphicon-list"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a  href="{{url('/competitions')}}">View Competitions</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/internships')}}">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">

                            <li><a href="{{url('/internships')}}">View Internships</a></li>


                        </ul>
                    </li>


                    <li >
                        <a href="{{url('/videos')}}">
                            <span class="glyphicon glyphicon-facetime-video"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/videos')}}">View Videos</a></li>
                        </ul>
                    </li>
                    <li >
                        <a href="{{url('/books')}}">
                            <span class="glyphicon glyphicon-book"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/books')}}">View Books</a></li>
                        </ul>
                    </li>


                </ul>
                <div class="side-widgets">
                    <div class="widgets-content">
                        <div class="text-center">
                            <i class="user-avatar fa fa-briefcase fa-5x"></i>
                            <div class="text-center avatar-name" style="margin-top:-40px;">
                                Internships
                            </div>
                        </div>



                        <div class="news-feed">
                            <div class="feed-header">TYPE</div>
                            <div class="feed-content">
                                <ul class="feed">


                                    @foreach($type as $item)
                                    <li>
                                        <a href="{{url('internships?type='.$item->type)}}">{{$item->type}}</a>
                                    </li>
                                    @endforeach

                                </ul>
                            </div>

                            <div class="feed-header">QUALIFICATION</div>
                            <div class="feed-content">
                                <ul class="feed">


                                    @foreach($qualification as $item)
                                        <li>
                                            <a href="{{url('internships?qualification='.$item->qualification)}}">{{$item->qualification}}</a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</aside>


<section id="body-container" class="animsition dashboard-page">
    @if( Session::has('success') )
        <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
    @endif


    @if( Session::has('error') )
        <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
    @endif


    @if(Input::has('type'))
    <h2 style="position:relative; left:50%; ">{{Input::get('type')}}</h2>
    @endif
        <br>

    @foreach($internships as $item)
        <div class="col-md-12">
            <div class="col-md-10">
                <div class="col-md-3">
                    <img class="img img-thumbnail" src="{{$item->photo}}">
                </div>
                <div class="col-md-9">
                    <h4>{{$item->company}}</h4>
                    <span>
                    {{$item->type}}|{{$item->qualification}}
                    </span>
                    <p>{{$item->description}}</p>
                    <span class="extra"></span>
                    <div class="grey">{{$item->contact}} | Ends {{$item->deadline}}</div>

                    @if(Auth::user()->role == "Student")
                    <form method="post" action="{{url('/apply/'. $item->intid  )}}">
                        {{csrf_field()}}
                    <button style="float: right;" class="right btn btn-success">Apply</button>
                    </form>
                    @endif
                </div>

            </div>
        </div>
        <div class="divider"></div>


    @endforeach


    <nav aria-label="Page navigation" class="col-md-12">
        <ul class="pagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
</section>

@endsection
