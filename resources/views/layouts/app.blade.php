<!doctype html>
<html>
<head>
    <title>Sarah's Project</title>
    <link href="{{url('css/semantic.min.css')}}" rel="stylesheet">
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/jquery.nicescroll.js')}}"></script>
    <script src="{{url('js/semantic.min.js')}}"></script>

</head>

<body class="ui grid container">

<div class="logoContainer">
    <a style="color:#421A01" href="{{url('/')}}">
        <img src="{{url('/images/logo.png')}}" class="logo">
    </a>
</div>
        @yield('content')



<footer>

<p> Copyright 2017. OIIE Nigeria </p>
</footer>

        <script>
            $(document).ready(function(){
//                $("html").niceScroll({cursorcolor:"#800000"});
//                $(".sidebar").niceScroll({cursorcolor:"#800000"});

                $('.ui.dropdown').dropdown();
            });

        </script>

</body>
</html>