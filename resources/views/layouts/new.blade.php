<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>	Admin Dashboard | OIIE </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{url('css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('css/app-green.css')}}" />
</head>
<body class="">

<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header text-center">
        <button type="button" class="navbar-toggle" id="showMenu" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="{{url('/')}}"> OIIE Startup Network </a>
    </div>
    <div class="collapse navbar-collapse">
        <img   src="{{url('images/logo-white.png')}}" style="margin-left:5px;">

        <ul class="nav navbar-nav">


        </ul>
        <ul class="nav navbar-nav pull-right navbar-right">




            <li class="dropdown admin-dropdown">

                <a href="{{url('/directory')}}"  role="button">
                    <p><span>Directory</span></p>
                </a>

            </li>
            <li class="dropdown admin-dropdown">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <p><span>Resources</span></p>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('/books')}}">Books</a></li>
                    <li><a href="{{url('/videos')}}">Videos</a></li>
                    <li><a href="{{url('/events')}}">Event</a></li>
                    <li><a href="{{url('/internships')}}">Internship</a></li>
                    <li><a href="{{url('/competitions')}}">Competition</a></li>

                    @if(Auth::user()->role == "Staff")
                        <li><a href="{{url('/view-int-report')}}">Internship Report</a></li>
                        <li><a href="{{url('/view-comp-report')}}">Competition Report</a></li>
                    @endif
                </ul>
            </li>




            <li class="dropdown admin-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <img src="{{Auth::user()->photo ? : url('images/flat-avatar.png')}}" class="topnav-img" alt=""><span class="hidden-sm">{{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    @if(Auth::user()->role == "Student")
                    <li><a href="{{url('/profile')}}">Profile</a></li>
                    @endif
                    <li><a href="{{url('/logout')}}">Logout</a></li>
                </ul>
            </li>
        </ul>


    </div>
    <ul class="nav navbar-nav pull-right hidd">
        <li class="dropdown admin-dropdown" dropdown on-toggle="toggled(open)">
            <a href class="dropdown-toggle animated fadeIn" dropdown-toggle><img src="{{Auth::user()->photo ? : url('images/flat-avatar.png')}}" class="topnav-img" alt=""></a>
            <ul class="dropdown-menu pull-right">
                <li><a href="{{url('/profile')}}">Profile</a></li>
                <li><a href="{{url('/logout')}}">Logout</a></li>
            </ul>
        </li>
    </ul>
</nav>



@yield('content')

<script src="{{url('vendor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
<script src="{{url('js/vendor.js')}}"></script>

<script>
    $(function(){
        $('#calendar2').fullCalendar({
            eventClick: function(calEvent, jsEvent, view) {
                alert('Event: ' + calEvent.title);
                alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                alert('View: ' + view.name);
            }
        });



    });

</script>
</body>


</html>
