<div class="ui sidebar visible inverted vertical menu">

    <a class="item sidebarProfileName">
        <img  src="{{url('/images/logo.png')}}">

        {{Auth::user()->name}}

    </a>

    <a class="item sidebarHeader">
        SKILLS/INTEREST
    </a>
    <a class="item">
        - Html
    </a>
    <a class="item">
        - Writing
    </a>
    {{--<a class="item">--}}
        {{--- Programming--}}
    {{--</a>--}}
    {{--<a class="item">--}}
        {{--- Baking--}}
    {{--</a>--}}
    <a class="item">
        - Ecommerce
    </a>

    <a class="item sidebarHeader">
        LINKS
    </a>
    <a class="item">
        - Facebook/Jane
    </a>
    <a class="item">
        - Twitter/Jane
    </a>
    <a class="item">
        - Instagram/jane
    </a>
    <a class="item">
        - LinkedIn/Jane
    </a>

    <a class="item sidebarHeader">
        CONTACT
    </a>

    <a class="item">
         {{Auth::user()->email}}
    </a>
    <a class="item">
        {{Auth::user()->phone}}
    </a>
</div>
