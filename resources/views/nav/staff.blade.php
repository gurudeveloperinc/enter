<div class="ui menu">

    <div class="header item">OIIE Startup Ecosystem</div>

    <div class="right menu">

        @if(Auth::guest())
            <a style="color:white;" class="item" href="{{url('/login')}}">Login</a>
        @else
        <div class="ui dropdown item">
            Directory <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Add Listing</a>
                <a class="item">View Directory</a>
            </div>
        </div>


        <div class="ui dropdown item">
            Resources <i class="dropdown icon"></i>
            <div class="menu">

                <div class="ui dropdown item">
                    Books <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{url('/add-book')}}">Add</a>
                        <a class="item" href="{{url('/books')}}">View</a>
                    </div>
                </div>


                <div class="ui dropdown item">
                    Videos <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{url('/add-video')}}">Add</a>
                        <a class="item" href="{{url('/videos')}}">View</a>
                    </div>
                </div>

                <div class="ui dropdown item">
                    Events <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{url('/add-event')}}">Add</a>
                        <a class="item" href="{{url('/events')}}">View</a>
                    </div>
                </div>


                <div class="ui dropdown item">
                    Internships <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{url('/add-internship')}}">Add</a>
                        <a class="item" href="{{url('/internships')}}">View</a>
                    </div>
                </div>


                <div class="ui dropdown item">
                    Competitions <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{url('/add-competition')}}">Add</a>
                        <a class="item" href="{{url('/competitions')}}">View</a>
                    </div>
                </div>

                <div class="ui dropdown item">
                   Reports <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{url('/view-reports')}}">View</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="ui dropdown item">
            Logged in as Sarah<i class="dropdown icon"></i>
            <div class="menu">

                <a class="item">Change Password</a>
                <a class="item" href="{{url('logout')}}">Logout</a>
            </div>
        </div>
        @endif
    </div>


</div>
