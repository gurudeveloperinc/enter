<div class="ui menu">

    <div class="header item">OIIE Startup Ecosystem</div>

    <div class="right menu">

        @if(Auth::guest())
            <a style="color:white;" class="item" href="{{url('/login')}}">Login</a>
        @else

           <a class="item">Directory</a>

            <div class="ui dropdown  item">
                Resources <i class="dropdown icon"></i>
                <div class="menu">

                    <a class="item" href="{{url('/books')}}">Books</a>
                    <a class="item" href="{{url('/videos')}}">Videos</a>
                    <a class="item" href="{{url('/events')}}">Events</a>
                    <a class="item" href="{{url('/internships')}}">Internships</a>
                    <a class="item" href="{{url('/competitions')}}">Competitions</a>


                </div>
            </div>

            <div class="ui dropdown link item">
                Logged in as Sarah<i class="dropdown icon"></i>
                <div class="menu">
                    <a class="item" href="{{url('/')}}">Profile</a>
                    <a class="item">Change Password</a>
                    <a class="item" href="{{url('logout')}}">Logout</a>
                </div>
            </div>
        @endif
    </div>


</div>
