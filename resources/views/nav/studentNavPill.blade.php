<ul class="nav nav-pills nav-stacked sidenav">
    <li >
        <a href="{{url('/home')}}">
            <span class="glyphicon glyphicon-home"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">
            <li><a href="{{url('home')}}">Dashboard</a></li>
        </ul>
    </li>

    <li >
        <a href="{{url('/directory')}}">
            <span class="glyphicon glyphicon-duplicate"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">
            <li><a href="{{url('directory')}}">View Directory</a></li>
        </ul>
    </li>

    <li >
        <a href="{{url('/events')}}">
            <span class="glyphicon glyphicon-book"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">
            <li><a href="{{url('/events')}}">View Events</a></li>
        </ul>
    </li>


    <li >
        <a href="{{url('/competitions')}}">
            <span class="glyphicon glyphicon-list"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">
            <li><a  href="{{url('/competitions')}}">View Competitions</a></li>
        </ul>
    </li>

    <li >
        <a href="{{url('/internships')}}">
            <span class="glyphicon glyphicon-briefcase"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">

            <li><a href="{{url('/internships')}}">View Internships</a></li>


        </ul>
    </li>


    <li >
        <a href="{{url('/videos')}}">
            <span class="glyphicon glyphicon-facetime-video"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">
            <li><a href="{{url('/videos')}}">View Videos</a></li>
        </ul>
    </li>
    <li >
        <a href="{{url('/books')}}">
            <span class="glyphicon glyphicon-book"></span>
        </a>
        <ul class="nested-dropdown animated fadeIn">
            <li><a href="{{url('/books')}}">View Books</a></li>
        </ul>
    </li>


</ul>