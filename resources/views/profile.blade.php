@extends('layouts.new')

@section('content')

@include('studentSidebar')
    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper">
            <div class="cover-wrapper">
                <div class="cover-photo" style="background-image: url( '{{Auth::user()->Student->cover}}' ) ;">
                    <div class="name-desg">
                        <h3>
                            DASHBOARD
                            <small>Student</small>
                        </h3>
                    </div>
                </div>
                <div class="profile-photo-warp">
                    <img class="profile-photo img-responsive img-circle" src="{{Auth::user()->photo}}" alt="">
                </div>

            </div>
            <div class="conter-wrapper">
                <div>
                    <div class="profile-body row" id="profile-items" style="margin:70px;">
                        <div class="col-sm-3">
                            <h4>ABOUT ME</h4>
                            <img  src="images/pic.png"alt="" width="185" height="180" >

                        </div>

                        <div class="col-sm-9" style="margin-top:55px;">
                            <p>{{Auth::user()->Student->bio}}</p>

                        </div>


                    </div>



                </div>

    </section>

@endsection