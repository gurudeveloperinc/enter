<aside id="sidebar">
    <div class="sidenav-outer">
        <div class="side-menu">
            <div class="menu-body">
                <ul class="nav nav-pills nav-stacked sidenav">
                    @if(Auth::user()->role == "Staff")
                    <li >
                        <a href="{{url('/staff')}}">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/staff')}}">Dashboard</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/update-directory')}}">
                            <span class="glyphicon glyphicon-duplicate"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('update-directory')}}">Update Directory</a></li>
                        </ul>
                    </li>


                    <li >
                        <a href="{{url('/add-event')}}">
                            <span class="glyphicon glyphicon-book"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/add-event')}}">Add Event</a></li>
                            <li><a href="{{url('/events')}}">View Event</a></li>
                        </ul>
                    </li>

                    @endif

                    <li >
                        <a href="{{url('/add-competition')}}">
                            <span class="glyphicon glyphicon-list"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a  href="{{url('/add-competition')}}">Add Competition</a></li>
                            <li><a  href="{{url('/competitions')}}">View Competition</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/add-internship')}}">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/add-internship')}}">Add Internship</a></li>
                            <li><a href="{{url('/internships')}}">View Internships</a></li>

                        </ul>
                    </li>


                        @if(Auth::user()->role == "Staff")
                    <li >
                        <a href="{{url('/add-video')}}">
                            <span class="glyphicon glyphicon-facetime-video"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/add-video')}}">Add video</a></li>
                            <li><a href="{{url('/videos')}}">Video</a></li>
                        </ul>
                    </li>
                    <li >
                        <a href="{{url('/add-book')}}">
                            <span class="glyphicon glyphicon-book"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/add-book')}}">Add Book</a></li>
                            <li><a href="{{url('/books')}}">View Book</a></li>
                        </ul>
                    </li>
                    <li >
                        <a href="{{url('/register')}}">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/register')}}">Add Admins</a></li>
                            <li><a href="{{url('/view-admins')}}">View Admins</a></li>

                        </ul>
                    </li>
                    <li >
                        <a href="{{url('/view-students')}}">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                             <li><a href="{{url('/view-students')}}">View Students</a></li>

                        </ul>
                    </li>

                            <li >
                                <a href="{{url('/view-int-report')}}">
                                    <span class="glyphicon glyphicon-briefcase"></span>
                                </a>
                                <ul class="nested-dropdown animated fadeIn">
                                    <li><a href="{{url('/view-int-report')}}">View Internship Reports</a></li>

                                </ul>
                            </li>

                            <li >
                                <a href="{{url('/view-comp-report')}}">
                                    <span class="glyphicon glyphicon-briefcase"></span>
                                </a>
                                <ul class="nested-dropdown animated fadeIn">
                                    <li><a href="{{url('/view-comp-report')}}">View Competition Reports</a></li>

                                </ul>
                            </li>

                        @endif


                        @if(Auth::user()->role == "Admin")

                            <li >
                                <a href="{{url('/view-com-applicants')}}">
                                    <span class="glyphicon glyphicon-briefcase"></span>
                                </a>
                                <ul class="nested-dropdown animated fadeIn">
                                    <li><a href="{{url('/view-com-applicants')}}">View Competition Applicants</a></li>

                                </ul>
                            </li>
                            <li >
                                <a href="{{url('/view-int-applicants')}}">
                                    <span class="glyphicon glyphicon-briefcase"></span>
                                </a>
                                <ul class="nested-dropdown animated fadeIn">
                                    <li><a href="{{url('/view-int-applicants')}}">View Internship Applicants</a></li>

                                </ul>
                            </li>
                        @endif

                </ul>
            </div>
        </div>
        <div class="side-widgets">
            <div class="widgets-content">
                <div class="text-center">
                    <a href="{{url('/profile')}}"><img src="{{Auth::user()->photo}}" class="user-avatar" /></a>
                    <div class="text-center avatar-name">
                        {{--{{Auth::user()->name}}--}}
                    </div>
                </div>

                <div class="calendar-container text-center" >
                    <div id="calendar2" class="fc-header-title"></div>
                </div>


            </div>
        </div>
    </div>
</aside>