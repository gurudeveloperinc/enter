<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>	Admin Dashboard | OIIE </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{url('css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('css/app-green.css')}}" />
</head>
<body class="">

<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header text-center">
        <button type="button" class="navbar-toggle" id="showMenu" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="{{url('/')}}"> OIIE Startup Network </a>
    </div>
    <div class="collapse navbar-collapse">


        <ul class="nav navbar-nav">


        </ul>
        <ul class="nav navbar-nav pull-right navbar-right">


    </ul>
</nav>



<body style="background-color:#27ae60;">
    <section id="body-container">
        <div class="conter-wrapper">
            <div class="row">
                <div class="col-md-10">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="position:relative; left:45%;">REGISTER</h3>

                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{url('/student/register')}}">
                                {{csrf_field()}}
                            <div class="row">

                                <div class="col-md-6">

                                        <div class="form-group">
                                            <label for="inputtext" class="col-sm-2 control-label">Full Name</label>
                                            <div class="col-sm-10">
                                                <input  required type="text" class="form-control" name="fullname">
                                                <p class="help-block">Example: Samuel Kio </p>
                                            </div>
                                        </div>
                                        <hr>


                                        <div class="form-group">

                                            <label for="inputtext" class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="email" required class="form-control" name="email">
                                                <p class="help-block">Example: Samuel.kio@regent.edu.gh</p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <label for="inputtext" class="col-sm-2 control-label">Skills</label>

                                            <div class="col-sm-2">
                                                <input type="text" name="skill1" class="form-control"  placeholder="Skill 1">
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" name="skill2" class="form-control" placeholder="Skill 2">
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="text" name="skill3" class="form-control" placeholder="Skill 3">

                                            </div>

                                        </div>
                                        <hr>
                                        <div class="row">
                                            <label for="inputtext" class="col-sm-2 control-label">Links</label>

                                            <div class="col-sm-12">
                                                <input type="text" name="facebook" class="form-control" placeholder="Facebook"> <br>
                                            </div>
                                            <div class="col-sm-12">
                                                <input type="text" name="twitter" class="form-control" placeholder="Twitter"> <br>
                                            </div>
                                            <div class="col-sm-12">
                                                <input type="text" name="other" class="form-control" placeholder="Other"> <br>

                                            </div>

                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="inputtext" class="col-sm-2 control-label">Contact Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="contact" required pattern="[+][0-9]+" class="form-control" id="placeholder" placeholder="Example +2348083664453">
                                                <p class="help-block">Important! Interested Companies need Credible Contact Information </p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label for="inputtext" class="col-sm-2 control-label">Bio</label>
                                            <div class="col-sm-10">
                                                <textarea name="bio" required maxlength="200" class="form-control rounded" placeholder="About you"></textarea>
                                                <p class="help-block">Promote yourself in 200 characters or less		</p>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-md-4" style="margin-left:75px;">
                                    <a href="{{url('/')}}">
                                    <img src="{{url('images/oiie.png')}}" >
                                    </a>
                                </div>

                                <div class="col-md-8">
                                    <hr>
                                    <div class="panel-body">
                                        <label for="inputtext" class="col-sm-2 control-label">Curriculum Vitae</label>
                                        <div class="col-sm-10">
                                            <p class="help-block">Upload your curriculum vitae to be eligible for internship and competitions	</p>
                                            <input name="cv" type="file" required  class="ui green button" class="btn btn-warning btn-rounded">Upload&nbsp;&nbsp;<i class="fa fa-book"></i>
                                        </div>


                                    </div>
                                    <div class="panel-body">
                                        <label for="inputtext" class="col-sm-2 control-label">Profile Picture</label>
                                        <div class="col-sm-10">
                                            <p class="help-block">Upload your desired ProfilePicture	</p>
                                            <input name="image" type="file"  class="ui green button" class="btn btn-warning btn-rounded">Upload&nbsp;&nbsp;<i class="fa fa-book"></i>
                                        </div>


                                    </div>
                                    <div class="panel-body">
                                        <label for="inputtext" class="col-sm-2 control-label">Header Photo</label>
                                        <div class="col-sm-10">
                                            <p class="help-block">Required dimensions	1020px950px</p>
                                            <input name="cover" type="file"  class="ui green button" class="btn btn-warning btn-rounded">Upload&nbsp;&nbsp;<i class="fa fa-book"></i>
                                        </div>


                                    </div>
                                    <hr>

                                    <div class="panel-body">
                                        <label for="inputtext" class="col-sm-6 control-label">This information is vital to your student profile and cannot be changed </label>
                                        <div class="col-sm-10">
                                            <button name="submit" type="submit"  class="btn btn-success btn-outline btn-rounded btn-lg" style="margin-left:500px;">Register</button>
                                        </div>
                                    </div>

                                    <hr>
                                </div>

                                </form>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>

    </section>
    <script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="js/vendor.js"></script>

    <script>
        $(function(){
            $('#calendar2').fullCalendar({
                eventClick: function(calEvent, jsEvent, view) {
                    alert('Event: ' + calEvent.title);
                    alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                    alert('View: ' + view.name);
                }
            });



            $('#rtlswitch').click(function() {
                console.log('hello');
                $('body').toggleClass('rtl');

                var hasClass = $('body').hasClass('rtl');

                $.get('/api/set-rtl?rtl='+ (hasClass ? 'rtl': ''));

            });
            $('.theme-picker').click(function() {
                changeTheme($(this).attr('data-theme'));
            });

            $('#showMenu').click(function() {
                $('body').toggleClass('push-right');

            });

        });
        function changeLanguage(lang){
            $.get('api/lang?lang='+lang);
            setTimeout(function() {
                window.location.reload(true);

            }, 550);
        }
        function changeTheme(theme) {

            $('<link>')
                    .appendTo('head')
                    .attr({type : 'text/css', rel : 'stylesheet'})
                    .attr('href', '/css/app-'+theme+'.css');

            $.get('api/change-theme?theme='+theme);
        }
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker').datetimepicker();
        });
    </script>
    </body>


</body>


</html>
