<aside id="sidebar">
    <div class="sidenav-outer">
        <div class="side-menu">
            <div class="menu-body">
                <ul class="nav nav-pills nav-stacked sidenav">
                    <li >
                        <a href="{{url('/home')}}">
                            <span class="glyphicon glyphicon-home"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('home')}}">Dashboard</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/directory')}}">
                            <span class="glyphicon glyphicon-duplicate"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('directory')}}">View Directory</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/events')}}">
                            <span class="glyphicon glyphicon-book"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/events')}}">View Events</a></li>
                        </ul>
                    </li>


                    <li >
                        <a href="{{url('/competitions')}}">
                            <span class="glyphicon glyphicon-list"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a  href="{{url('/competitions')}}">View Competitions</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="{{url('/internships')}}">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">

                            <li><a href="{{url('/internships')}}">View Internships</a></li>


                        </ul>
                    </li>


                    <li >
                        <a href="{{url('/videos')}}">
                            <span class="glyphicon glyphicon-facetime-video"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/videos')}}">View Videos</a></li>
                        </ul>
                    </li>
                    <li >
                        <a href="{{url('/books')}}">
                            <span class="glyphicon glyphicon-book"></span>
                        </a>
                        <ul class="nested-dropdown animated fadeIn">
                            <li><a href="{{url('/books')}}">View Books</a></li>
                        </ul>
                    </li>


                </ul>
            </div>
        </div>
        <div class="side-widgets">
            <div class="widgets-content">
                <div class="text-center">
                    <a href="{{url('/profile')}}"><img src="{{Auth::user()->photo}}" class="user-avatar" /></a>
                    <div class="text-center avatar-name">
                        {{Auth::user()->name}}
                    </div>
                </div>



                <div class="calendar-container text-center" >
                    <div class="fc-header-title"><strong>SKILLS/INTEREST</strong></div>

                    <p class="align center">

                        @if(Auth::user()->role == "Student")
                        {{Auth::user()->Student->skill1}}
                        @endif
                    </p>
                    <p class="align center">
                        @if(Auth::user()->role == "Student")
                            {{Auth::user()->Student->skill2}}
                        @endif

                    </p>
                    <p class="align center">
                        @if(Auth::user()->role == "Student")
                            {{Auth::user()->Student->skill3}}
                        @endif

                    </p>

                </div>
                <br>

                <div class="calendar-container text-center" >
                    <div class="fc-header-title"><strong>SOCIAL LINKS</strong></div>

                    <p class="align center">
                        @if(Auth::user()->role == "Student")
                            {{Auth::user()->Student->facebook}}
                        @endif

                    </p>
                    <p class="align center">
                        @if(Auth::user()->role == "Student")
                            {{Auth::user()->Student->twitter}}
                        @endif
                    </p>
                    <p class="align center">
                        @if(Auth::user()->role == "Student")
                            {{Auth::user()->Student->others}}
                        @endif

                    </p>

                </div>
                <br>
                <div class="calendar-container text-center" >
                    <div class="fc-header-title"><strong>CONTACT</strong></div>


                    <p>
                        {{Auth::user()->phone}} <br>
                        {{Auth::user()->email}} <br>

                        <a class="btn btn-info" href="{{Auth::user()->Student->cv}}">Download Cv</a>
                    </p>
                </div>


            </div>
        </div>
    </div>
</aside>