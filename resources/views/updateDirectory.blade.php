@extends('layouts.new')

@section('content')

    @include('staffSidebar')

    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper">
            @if( Session::has('success') )
                <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
            @endif


            @if( Session::has('error') )
                <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
            @endif



            <div class="col-md-12" style="width:70%; margin-left:160px;">
                <div class="panel panel-success">

                    <div class="panel-heading">
                        <h3 class="panel-title">Add Directory
                            <div class="panel-control pull-right">
                                <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                <a class="panelButton"><i class="fa fa-minus"></i></a>
                                <a class="panelButton"><i class="fa fa-remove"></i></a>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" enctype="multipart/form-data" action="{{url('/update-directory')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <div class="row>">

                                    <input type="file" name="file">

                                </div>
                            </div>

                            <button type="submit" class="btn btn-success">Add </button>
                        </form>
                    </div>

                </div>


            </div>
        </div>

    </section>

@endsection