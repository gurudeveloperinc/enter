@extends('layouts.new')

@section('content')

    @if(Auth::user()->role == "Student")
        @include('studentSidebar')
    @endif

    @if(Auth::user()->role == "Admin")
        @include('staffSidebar')
    @endif



    <section id="body-container" class="animsition dashboard-page">
        <div class="row">

            @foreach($videos as $item)
            <div class="col-sm-4 col-md-3">
                <div class="thumbnail">
                    <img src="{{$item->photo}}" >
                    <div class="caption">
                        <h3>{{$item->title}}</h3>
                        <p>{{$item->description}}</p>
                        <p><a href="{{$item->url}}" class="btn btn-primary" role="button" download>Download</a> </p>
                    </div>
                </div>
            </div>
            @endforeach


        </div>


        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </section>

@endsection