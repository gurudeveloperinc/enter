@extends('layouts.new')

@section('content')
    @if(Auth::user()->role == "Student")
        @include('studentSidebar')

    @else
        @include('staffSidebar')


    @endif

    <section id="body-container" class="animsition dashboard-page">
        <pageheader pagename="Table" subtitle="Bootstrap UI Elements"></pageheader>
        <div class="conter-wrapper">
            <div class="row">
                <div class="col-md-12">
                    @if( Session::has('success') )
                        <div class="alert alert-success" align="center">{{Session::get('success')}}</div>
                    @endif


                    @if( Session::has('error') )
                        <div class="alert alert-error" align="center">{{Session::get('error')}}</div>
                    @endif


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Students
                                <div class="panel-control pull-right">
                                    <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                    <a class="panelButton"><i class="fa fa-minus"></i></a>
                                    <a class="panelButton"><i class="fa fa-remove"></i></a>
                                </div>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Qualification</th>
                                    <th>Contact Email</th>
                                    <th>Contact Number</th>
                                    <th>Applied On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($competition as $item)
                                    <tr>
                                        <td>

                                            <a href="{{url('/view-profile/'. $item->uid)}}">
                                                {{$item->Student->name}}
                                            </a>

                                        </td>
                                        <td>{{$item->Competition->type}}</td>
                                        <td>{{$item->Competition->qualification}}</td>
                                        <td>{{$item->Student->email}}</td>
                                        <td>{{$item->Student->phone}}</td>
                                        <td>{{$item->Student->created_at}}</td>
                                        <td>
                                            <a href="{{url('/approve-comp/' . $item->aid)}}" class="btn btn-success">Approve</a>
                                            <a href="{{url('/reject-comp/' . $item->aid)}}" class="btn btn-danger">Reject</a>
                                        </td>

                                    </tr>
                                @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        </div>

        </div>

        </div>
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
@endsection