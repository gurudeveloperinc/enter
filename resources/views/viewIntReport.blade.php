<html>
<head>

    <link href="{{url('css/app.css')}}" rel="stylesheet" type="text/css">

</head>
<body>
<div align="center" class="col-md-12 main " >
    <div>
        <img src="{{url('/images/logo.png')}}" style="height: 100px;" class="logo">
        <h3 class="logoHeader" style="font-size: 30px;color:#8B8E45">OIIE STARTUP NETWORK</h3>
    </div>

    <style>
        body{
            background-color: white;
        }
        td,th{
            text-align: center;
        }

    </style>

    <h3 style="color:#8B8E45">INTERNSHIP APPLICATION REPORT</h3>
    <h3>Generated on: {{\Carbon\Carbon::now()->toDateTimeString()}}</h3>
    <table class="table ">
        <thead>
        <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Qualification</th>
            <th>Contact Email</th>
            <th>Contact Number</th>
            <th>Applied On</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($internships as $item)
            <tr>
                <td>{{$item->Student->name}}</td>
                <td>{{$item->Internship->type}}</td>
                <td>{{$item->Internship->qualification}}</td>
                <td>{{$item->Student->email}}</td>
                <td>{{$item->Student->phone}}</td>
                <td>{{$item->Student->created_at}}</td>
                <td>
                    {{$item->status}}
                </td>

            </tr>
        @endforeach



        </tbody>


</div>
</body>
</html>