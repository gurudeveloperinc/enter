@extends('layouts.new')

@section('content')

    <aside id="sidebar">
        <div class="sidenav-outer">
            <div class="side-menu">
                <div class="menu-body">
                    <ul class="nav nav-pills nav-stacked sidenav">
                        <li >
                            <a href="{{url('/home')}}">
                                <span class="glyphicon glyphicon-home"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">
                                <li><a href="{{url('home')}}">Dashboard</a></li>
                            </ul>
                        </li>

                        <li >
                            <a href="{{url('/directory')}}">
                                <span class="glyphicon glyphicon-duplicate"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">
                                <li><a href="{{url('directory')}}">View Directory</a></li>
                            </ul>
                        </li>

                        <li >
                            <a href="{{url('/events')}}">
                                <span class="glyphicon glyphicon-book"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">
                                <li><a href="{{url('/events')}}">View Events</a></li>
                            </ul>
                        </li>


                        <li >
                            <a href="{{url('/competitions')}}">
                                <span class="glyphicon glyphicon-list"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">
                                <li><a  href="{{url('/competitions')}}">View Competitions</a></li>
                            </ul>
                        </li>

                        <li >
                            <a href="{{url('/internships')}}">
                                <span class="glyphicon glyphicon-briefcase"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">

                                <li><a href="{{url('/internships')}}">View Internships</a></li>


                            </ul>
                        </li>


                        <li >
                            <a href="{{url('/videos')}}">
                                <span class="glyphicon glyphicon-facetime-video"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">
                                <li><a href="{{url('/videos')}}">View Videos</a></li>
                            </ul>
                        </li>
                        <li >
                            <a href="{{url('/books')}}">
                                <span class="glyphicon glyphicon-book"></span>
                            </a>
                            <ul class="nested-dropdown animated fadeIn">
                                <li><a href="{{url('/books')}}">View Books</a></li>
                            </ul>
                        </li>


                    </ul>
                </div>
            </div>
            <div class="side-widgets">
                <div class="widgets-content">
                    <div class="text-center">
                        <a href="{{url('/profile')}}"><img src="{{$student->photo}}" class="user-avatar" /></a>
                        <div class="text-center avatar-name">
                            {{--{{$student->name}}--}}
                        </div>
                    </div>



                    <div class="calendar-container text-center" >
                        <div class="fc-header-title"><strong>SKILLS/INTEREST</strong></div>

                        <p class="align center">

                            @if($student->role == "Student")
                                {{$student->Student->skill1}}
                            @endif
                        </p>
                        <p class="align center">
                            @if($student->role == "Student")
                                {{$student->Student->skill2}}
                            @endif

                        </p>
                        <p class="align center">
                            @if($student->role == "Student")
                                {{$student->Student->skill3}}
                            @endif

                        </p>

                    </div>
                    <br>

                    <div class="calendar-container text-center" >
                        <div class="fc-header-title"><strong>SOCIAL LINKS</strong></div>

                        <p class="align center">
                            @if($student->role == "Student")
                                {{$student->Student->facebook}}
                            @endif

                        </p>
                        <p class="align center">
                            @if($student->role == "Student")
                                {{$student->Student->twitter}}
                            @endif
                        </p>
                        <p class="align center">
                            @if($student->role == "Student")
                                {{$student->Student->others}}
                            @endif

                        </p>

                    </div>
                    <br>
                    <div class="calendar-container text-center" >
                        <div class="fc-header-title"><strong>CONTACT</strong></div>


                        <p>
                            {{$student->phone}} <br>
                            {{$student->email}} <br>

                            <a class="btn btn-info" href="{{$student->Student->cv}}">Download Cv</a>
                        </p>
                    </div>


                </div>
            </div>
        </div>
    </aside>
    <section id="body-container" class="animsition dashboard-page">
        <div class="conter-wrapper">
            <div class="cover-wrapper">
                <div class="cover-photo" style="background-image: url( '{{$student->Student->cover}}' ) ;">
                    <div class="name-desg">
                        <h3>
                            {{strtoupper($student->name)}}
                            <small>{{$student->role}}</small>
                        </h3>
                    </div>
                </div>
                <div class="profile-photo-warp">
                    <img class="profile-photo img-responsive img-circle" src="{{$student->photo}}" alt="">
                </div>

            </div>
            <div class="conter-wrapper">
                <div>
                    <div class="profile-body row" id="profile-items" style="margin:70px;">
                        <div class="col-sm-3">
                            <h4>ABOUT ME</h4>
                            <img  src="images/pic.png"alt="" width="185" height="180" >

                        </div>

                        <div class="col-sm-9" style="margin-top:55px;">
                            <p>{{$student->Student->bio}}</p>

                        </div>


                    </div>



                </div>

    </section>

@endsection