<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>OIIE HOME PAGE</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="white" role="navigation">
    <div class="nav-wrapper container">
        <a href="http://www.ictinnovation.gov.ng">
        <img src="{{url('images/oiie1.png')}}" id="logo-container"  class="brand-logo">
        </a>
        <ul class="right hide-on-med-and-down">
            @if(!Auth::guest())
                <li><a href="{{url('/home')}}">Dashboard</a></li>
            @else
            <li><a href="{{url('/login')}}">Sign in</a></li>
            <li><a href="{{url('/student/register')}}">Sign up</a></li>
            @endif
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a href="{{url('/login')}}">Sign in</a></li>
            {{--<li><a href="{{url('/student/register')}}">Sign up</a></li>--}}
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text text-lighten-2">OIIE Start up Network</h1>
            <div class="row center">
                <h4 class="header col s12 light"><i>“Innovation is seeing what everybody has seen
                        and thinking what nobody has thought”</i></h4>
            </div>
            <div class="row center">
                <h5 class="header col s12 light"><i>Dr. Albert Szent-Gyorgyi
                        (discovered Vitamin C)</i></h5>
            </div>
            <br><br>

        </div>
    </div>
    <div class="parallax"><img src="{{url('background1.jpg')}}" alt="Unsplashed background img 1"></div>
</div>


<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><i class="material-icons">group</i></h2>
                    <h5 class="center">Startup Friday</h5>

                    <p class="light">StartUP Friday is a program of Office for ICT Innovation and Entrepreneurship (OIIE) designed to promote and encourage new start-ups and foster relationships with investors, mentors and successful start-ups. StartUP Friday is a “meet-up” designed to bridge the gap that exist amongst  startups, investors, mentors, buyers and other players in the Nigerian entrepreneurial ecosystem.</p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><i class="material-icons">group</i></h2>
                    <h5 class="center">FutureHACK</h5>

                    <p class="light">FutureHACK is the future – hacking the current problems to produce solutions that can disrupt bad lifestyle or amplify good ones. It is about promoting out-of-this world, futuristic but thought-provoking ideas and/or solutions that can be funded for further research and funding into becoming ventures.</p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center brown-text"><i class="material-icons">settings</i></h2>
                    <h5 class="center">StartUP Clinic</h5>

                    <p class="light">StartUP Clinic is a SMART gathering for a few startup, mentors, successful entrepreneurs, investors, Industry specialist, business consultants and hub operators in an interactive and exchange mode to give maximum attention to startup founders with the goal of solving problems and challenges their business are facing.</p>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="container">
    <div class="section">

        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>Contact Us</h4>
                <p class="left-align light">+234-(0)-909-788-8885. | info@ictinnovation.gov.ng
                    5 Donau Crescent, Off Amazon Street, Maitama – Abuja, Nigeria</p>
            </div>
        </div>

    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light">We invest in asipiring and potential entreprenuers around Nigeria</h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="{{url('background3.jpg')}}" alt="Unsplashed background img 3"></div>
</div>

<footer class="page-footer" style="background-color: #27ae60;">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">OIIE Start up Network</h5>
                <p class="grey-text text-lighten-4">We invest in asipiring and potential entreprenuers around Nigeria</p>


            </div>

            <div class="col l3 s12">
                <h5 class="white-text">Connect</h5>
                <ul>
                    <li><a class="white-text" href="#!">Phone Number</a></li>
                    <li><a class="white-text" href="#!">Email</a></li>
                    <li><a class="white-text" href="#!">Skype</a></li>
                    <li><a class="white-text" href="#!">Maitama, Abuja</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="brown-text text-lighten-3" href="#">Sarah Shaibu</a>
        </div>
    </div>
</footer>


<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="{{url('js/materialize.js')}}"></script>
<script src="{{url('js/init.js')}}"></script>

</body>
</html>
