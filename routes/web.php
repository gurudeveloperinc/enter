<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::get('/', 'PublicController@index');
Route::get('/home', 'HomeController@index');
Route::get('/staff','HomeController@staff');

Route::get('/logout','HomeController@logout');

Route::get('/student/register','PublicController@getStudentRegister');
Route::post('/student/register','PublicController@postStudentRegister');

Route::get('/profile','HomeController@getProfile');

Route::get('/books','HomeController@getBooks');
Route::get('/books/{tag}','HomeController@getBooksByTags');
Route::get('/add-book','HomeController@getAddBook');


Route::get('/events','HomeController@getEvents');
Route::get('/events/{location}','HomeController@getEventsByLocation');
Route::get('/add-event','HomeController@getAddEvent');


Route::get('/internships','HomeController@getInternships');
Route::get('/add-internship','HomeController@getAddInternship');


Route::get('/add-competition','HomeController@getAddCompetition');
Route::get('/competitions','HomeController@getCompetitions');


Route::get('/add-video','HomeController@getAddVideo');
Route::get('/videos','HomeController@getVideos');

Route::get('/directory','HomeController@getDirectory');
Route::get('/update-directory','HomeController@getUpdateDirectory');

Route::get('/view-profile/{uid}','HomeController@viewProfile');
Route::get('/view-students','HomeController@viewStudents');

Route::get('/view-admins','HomeController@viewAdmins');

Route::get('/delete/{uid}','HomeController@deleteUser');

Route::get('/admin','HomeController@admin');
Route::get('/view-com-applicants','HomeController@viewCompApplicants');
Route::get('/view-int-applicants','HomeController@viewIntApplicants');
Route::get('/internship-more/{id}','HomeController@internshipMore');

Route::get('/approve-int/{intid}','HomeController@approveInt');
Route::get('/approve-comp/{compid}','HomeController@approveComp');

Route::get('/reject-int/{intid}','HomeController@rejectInt');
Route::get('/reject-comp/{compid}','HomeController@rejectComp');

Route::get('/view-int-report','HomeController@viewIntPDF');
Route::get('/view-comp-report','HomeController@viewCompPDF');

Route::post('/add-book','HomeController@postAddBook');
Route::post('/add-event','HomeController@postAddEvent');
Route::post('/add-internship','HomeController@postAddInternship');
Route::post('/add-competition','HomeController@postAddCompetition');

Route::post('/apply/{intid}','HomeController@apply');
Route::post('/apply-comp/{compid}','HomeController@applyComp');


Route::post('/update-directory','HomeController@postUpdateDirectory');
